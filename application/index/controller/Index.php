<?php
namespace app\index\controller;

use think\App;
use think\Controller;
use think\Db;

class Index extends Controller
{
    public function index()
    {
//        $this->insertCountry();
//        $result=Db::name("region")->where("id","<=","45063")->delete();
    }

    //导入国家
    private function insertCountry()
    {
        $sysCountry=Db::name("sysarea")->where("vc_area_name","<>","AREA-234")->where("l_level",0)->select();
        $countries=[];
        foreach($sysCountry as $k=>$v){
            //查出每个国家的具体信息
            $country=Db::name("syslang")
                ->where("vc_code",$v["vc_area_name"])
                ->where("e_lang_type","en_US")
                ->find();
            $countries[]=$country;
            //将所有国家插入到s_region表内
            $data=[
                "pid"=>0,
                "name"=>$country["vc_name"],
                "level"=>1,
            ];
            $country_id=Db::name("region")->insertGetId($data);

            //查出每个国家下面的省数据（中国）或州数据
            $sys_province=Db::name("sysarea")
                ->where("vc_area_name","like","%".$country["vc_code"]."%")
                ->where("l_level",1)
                ->select();
            $provinces=[];
            foreach ($sys_province as $pk=>$pv){
                //查出每个省或州的具体信息
                $province=Db::name("syslang")->where("vc_code",$pv["vc_area_name"])
                    ->where("e_lang_type","en_US")->find();
                $provinces[]=$province;
                $province_data=[
                    "pid"=>$country_id,
                    "name"=>$province["vc_name"],
                    "level"=>2,
                ];
                $province_id=Db::name("region")->insertGetId($province_data);


                //查出省或州下面的城市
                $sys_city=Db::name("sysarea")->where("vc_area_name","like","%".$province["vc_code"]."%")
                    ->where("l_level",2)->select();
                $cities=[];
                foreach ($sys_city as $ck=>$cv){
                    //查出每个城市的具体信息
                    $city=Db::name("syslang")->where("vc_code",$cv["vc_area_name"])
                        ->where("e_lang_type","en_US")->find();
                    $cities[]=$city;
                    $city_data=[
                        "pid"=>$province_id,
                        "name"=>$city["vc_name"],
                        "level"=>3
                    ];
                    $city_id=Db::name("region")->insertGetId($city_data);
                }

            }

        }

    }
    //导入城市
    private function insertCity()
    {

    }
    //导入地区
    private function insertArea()
    {

    }


    public function second()
    {
        $sysarea=Db::name("sysarea")
            ->where("vc_area_name","like","%AREA-234%")
            ->where("l_level",1)
            ->select();
//        $usa=Db::name("region")->where("")

        foreach ($sysarea as $k=>$v){
            //查出每个州下面的城市信息
            $city=Db::name("sysarea")
                ->where("vc_area_name","like","%".$v["vc_area_name"]."%")
                ->where("l_level",2)
                ->select();
            dump($city);
            foreach ($city as $c=>$t){
                $city_name=Db::name("syslang")->where("vc_code",$t["vc_area_name"])->where("e_lang_type","en_US")->find();
                dump($city_name);
                $city_in_region=Db::name("region")->where("name",$city_name["vc_name"])->find();
                dump($city_in_region);exit;
                $country=Db::name("syslang")
                    ->where("vc_code",$t["vc_area_name"])
                    ->where("e_lang_type","en_US")
                    ->find();
                dump($country);exit;
                $data=[
                    "pid"=>$t["vc_area_id"],
                    "name"=>$country["vc_name"],
                    "level"=>3,

                ];
                $result=Db::name("region")->insertGetId($data);
            }

//            dump($country);

//            $result=Db::name("region")->insertGetId($data);
        }

//        $result=Db::name("region")->insertGetId($data);
    }

    private function insertUsa()
    {
        $cities=Db::name("region")->where("pid","45064")->select();
        foreach ($cities as $k=>$v){
            $city=Db::name("syslang")->where("vc_name",$v["name"])->find();
            $country=Db::name("syslang")
                ->where("vc_code","like","%".$city["vc_code"]."%")
                ->where("vc_code","<>",$city["vc_code"])
                ->where("e_lang_type","en_US")
                ->select();
            foreach ($country as $co=>$tr){
                $data=[
                    "pid"=>$v["id"],
                    "name"=>$tr["vc_name"],
                    "level"=>3,
                ];
//                $result=Db::name("region")->insertGetId($data);
            }
        }
    }

}
